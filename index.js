const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const app = express();


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); // this will allow our hosted front-end app to send requests

//Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);

// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-yumang.2kvxe2i.mongodb.net/E-commerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));


// Since we're hosting this API in the cloud, the port to be used should be flexbile hence, the use of the process.env.PORT which will take port that the cloud server uses if the 'port' variable isn't available.
app.listen(process.env.PORT || port, () => {
	console.log(`Capstone 2 E-commerce-API is now running at localhost:${process.env.PORT || port}`);
});

module.exports = app;