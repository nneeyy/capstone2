const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.addProduct = (request, response) => {

	let new_product = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    });

	// Saves the created object to our database
	return new_product.save().then((saved_product, error) => {

		// Product creation successful
		if (error) {
			return response.send(false);

		// Product creation failed
		} else {
			return response.send(true);
		}
	})
	.catch(error => response.send(error))
};




// retrieving all products
module.exports.getAllProducts = (request, response) => {

	return Product.find({}).then(result => {

		return response.send(result);
	})
}


// retrieving all active products
module.exports.getAllActiveProducts = (request, response) => {

	return Product.find({isActive: true})
	.then(result => {

		return response.send(result);
	})
}


// Controller action to search for products by product name
module.exports.searchProductsByName = async (request, response) => {
  try {
    const { productName } = request.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    response.json(products);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: 'Internal Server Error' });
  }
};



module.exports.searchProductsByPriceRange = async (request, response) => {
	try {
		const { minPrice, maxPrice } = request.body;
  
          // Find products within the price range
		const products = await Product.find({
        price: { $gte: minPrice, $lte: maxPrice }
    	});

    	response.status(200).json({ products });
    } catch (error) {
      response.status(500).json({ error: 'An error occurred while searching for products' });
    }
   };




module.exports.getProduct = (request, response) => {

	return Product.findById(request.params.productId)
	.then(result => {
		console.log(result)
		return response.send(result)
	})
	.catch(error => response.send(error))
};




module.exports.updateProduct = (request, response) => {

	let updated_product_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.productId, updated_product_details)
	.then((product, error) => {

		if(error) {
			return response.send(false);
		
		} else {
			
			return response.send(true);
		}
	})
	.catch(error => response.send(error))
};




// Route to archive a product (Admin only)
module.exports.archiveProduct = (request, response) => {

	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(request.params.productId, updateActiveField)
	.then((product, error) => {

		//product archived successfully
		if(error){
			return response.send(false)

		// failed
		} else {
			return response.send(true)
		}
	})
	.catch(error => response.send(error))

};


// Activate a product (Admin only)
module.exports.activateProduct = (request, response) => {

	let updateActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(request.params.productId, updateActiveField)
	.then((product, error) => {

		//product archived successfully
		if(error){
			return response.send(false)

		// failed
		} else {
			return response.send(true)
		}
	})
	.catch(error => response.send(error))

};