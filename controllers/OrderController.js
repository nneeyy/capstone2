const Order = require('../models/Order.js');
const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;




// checkout order
module.exports.directCheckout = (request_body) => {

    let new_order = new Order({

        userId: request_body.userId,
        productId: request_body.productId,
        productName: request_body.productName,
        quantity: request_body.quantity,
        totalAmount: request_body.price * request_body.quantity
    });

    return new_order.save().then((checkout_order, error) => {

        if(error){
            return {
                message: error.message
            };
        }
        return {
            
            message: true
        };
    }).catch(error => console.log(error));

}






// Retrieve user orders
module.exports.getOrders = (request, response) => {

    return Order.find({}).then(result => {

        return response.send(result);
    })
};