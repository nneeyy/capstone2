const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.checkEmailExists = (request_body) => {

	return User.find({email: request_body.email}).then((result, error) => {

		if (error) {

			return error.message
		}

		if (result.length <= 0) {

			return "Email is not registered yet.";
		}

		return "User email already exists.";
	})
}



module.exports.registerUser = (request_body) => {

	let new_user = new User ({

		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {

		if (error) {

			return false;

		} else {

			return true;
		}

		

	}).catch (error => console.log(error));
}



// module.exports.loginUser = (request, response) => {

// 	return User.findOne({email: request.body.email}).then(result => {
	
// 		if (result == null) {

// 			return response.send({

// 				message: "The user is not registered yet."
// 			})
// 		}

// 		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

// 		if (isPasswordCorrect) {

// 			return response.send({accessToken: auth.createAccessToken(result)});

// 		} else {

// 			return response.send({

// 				message: 'Password is incorrect.'
// 			})
// 		}
// 	}).catch(error => response.send(error));
// }


module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		
		console.log(result);
		if(result == null) {
			return response.send(false)
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect) {
				
				return response.send({access: auth.createAccessToken(result)})

			} else {
				
				return response.send(false);
			}
		}
	}).catch(error => response.send(error));

};



// Function to reset the password
module.exports.resetPassword = async (request, response) => {
  try {
    const { newPassword } = request.body;
    const { id } = request.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    response.status(200).json(true);
  } catch (error) {
    console.error(error);
    response.status(500).json({ message: 'Internal server error' });
  }
};



module.exports.updateProfile = async (request, response) => {
  try {
    // Get the user ID from the authenticated token
    const userId = request.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = request.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    response.json(updatedUser);
  } catch (error) {
    console.error(error);
    response.status(500).json({ message: 'Failed to update profile' });
  }
};



module.exports.getAllUsers = (request, response) => {

	return User.find({}).then(result => {

		return response.send(result);
	})
}



// module.exports.getProfile = (request_body) => {
//     return User.findOne({ _id: request_body.id })
//         .then(user => {
//             if (!user) {
//                 return "User does not exist";
//             }
            
//             user.password = "";
//             return user;
//         })
//         .catch(error => error.message);
// };


module.exports.getProfile = (request, response) => {


	return User.findById(request.user.id).then(result => {

		result.password = "";

		// Returns the user information with the password as an empty string
		return response.send(result);

	})
	.catch(error => response.send(error))
};



module.exports.setUserAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id, { isAdmin: true })
	.then(() => response.send("User succesfully set to admin!"))
	.catch(error => response.send(error.message));
};


module.exports.addToCart = async (request, response) => {

	if (request.user.isAdmin) {

		return response.send('Action forbidden');
	}

	let isUserUpdated = await User.findById(request.user.id)
	.then(user => {

		let new_items_to_cart = {

			productId: request.body.productId,
			quantity: request.body.quantity,
			price: request.body.price,
			subtotal: request.body.price * (request.body.quantity || 1)

		}

		user.cart.push(new_items_to_cart);

		return user.save().then(updated_user => true)
		.catch(error => error.message);
	})

	if (isUserUpdated !== true) {

		return response.send({message: isUserUpdated});
	}

	if (isUserUpdated) {

		return response.send(true);
	}

}









module.exports.getCartItems = (request, response) => {

	User.findById(request.user.id)
	.then(result => response.send(result.cart))
	.catch(error => response.send(error.message))
}



module.exports.checkoutCart = (request, response) => {
    User.findById(request.user.id)
        .populate('cart.productId')
        .then(user => {
            if (!user) {
                return response.send("User not found.");
            }

            if (user.cart.length === 0) {
                return response.send("Cart is empty.");
            }

            // Calculate total amount
            const totalAmount = user.cart.reduce((total, item) => total + item.subtotal, 0);

            const order = new Order({
                userId: user.id,
                products: user.cart.map(item => {
                    return {
                        productId: item.productId._id,
                        quantity: item.quantity,
                        price: item.price
                    };
                }),
                totalAmount: totalAmount
            });

            order.save()
                .then(savedOrder => {
                    user.cart = [];
                    user.totalAmount = 0;

                    user.save()
                        .then(() => {
                            response.send("Order placed successfully!");
                        })
                        .catch(error => {
                            response.send(error.message);
                        });
                })
                .catch(error => {
                    response.send(error.message);
                });
        })
        .catch(error => {
            response.send(error.message);
        });
};









