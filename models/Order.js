const mongoose = require('mongoose');


const order_schema = new mongoose.Schema({

    userId: {

        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "User Id is required"]
    },

    userFullName: {
        type: String,
        required: false
    },
    products: [
        {
            productId: {
                type : mongoose.Schema.Types.ObjectId,
                ref: "Product",
                required: [true, "ProductId is required!"]
            },
            productName: {
                type: String,
                required: false
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required!"]
            }
        }
    ],

    totalAmount: {
        type: Number,
        required: [true, "Amount is required"]
    },

    purchasedOn: {
        type: Date,
        default:  new Date()
    }
});



module.exports = mongoose.model("Order", order_schema);
