const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;




// Add/create a product
router.post("/add", verify, verifyAdmin, ProductController.addProduct);



// Retrieve all products
router.get('/all', (request, response) => {

	ProductController.getAllProducts(request, response);
});



// Retrieve all active products
router.get('/', (request, response) => {

	ProductController.getAllActiveProducts(request, response);
})


// Route to search for products by product name
router.post('/search', ProductController.searchProductsByName);


//[ACTIVITY] Search Products By Price Range
router.post('/searchByPrice', ProductController.searchProductsByPriceRange);



router.get("/:productId", ProductController.getProduct);


// Route for updating a product (Admin)
router.put("/:productId", verify, verifyAdmin, ProductController.updateProduct);



// Route to archiving a product (Admin)
router.put("/:productId/archive", verify, verifyAdmin, ProductController.archiveProduct);



// Route to activating a product (Admin)
router.put("/:productId/activate", verify, verifyAdmin, ProductController.activateProduct);





module.exports = router;