const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;



// Check if email exists
router.post('/check-email', (request, response) => {

	UserController.checkEmailExists(request.body).then((result) => {

		response.send(result);
	});
});



// Route for user registration
router.post("/register", (request, response) => {

	UserController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});



// Login user
router.post('/login', (request, response) => {

	UserController.loginUser(request, response);
})

// router.post("/login", UserController.loginUser);


// Route for resetting the password
router.put('/reset-password', verify, UserController.resetPassword);


// Update user profile route
router.put('/profile', verify, UserController.updateProfile);


// Get all users
router.get('/all', (request, response) => {

	UserController.getAllUsers(request, response);
});



// Route for retrieving authenticated user details
router.get("/details", verify, UserController.getProfile)


// Set user as admin (admin only)
router.put('/:id/update-role', verify, verifyAdmin, (request, response) => {

	UserController.setUserAdmin(request, response);
})


// Add to cart attempt 2.0
router.post('/cart/add', verify, (request, response) => {

	UserController.addToCart(request, response);
})


// Get user cart items
router.get('/cart', verify, (request, response) => {

	UserController.getCartItems(request, response);
})


router.post("/addcart/:id", async(req, res) => {
	try {

		const {id} = req.params.id;
		const cart = Product.findOne({id:id});

		console.log(cart + "cart value")

	} catch (error) {

	}
})



module.exports = router;