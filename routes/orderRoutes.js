const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const ProductController = require('../controllers/ProductController.js');
const UserController = require('../controllers/UserController.js')
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;



// checkout order
router.post('/checkout', verify, (request, response) => {

    OrderController.directCheckout(request.body).then((result) => {
        
        response.send(result);
    })
})


// Retrieve user orders
router.get('/', verify, (request, response) => {

    OrderController.getOrders(request, response);
});





module.exports = router;